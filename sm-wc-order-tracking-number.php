<?php


/**
 * Plugin Name: WooCommerce Order Tracking Number
 * Plugin URI: https://samm.xyz
 * Description: Add WooCommerce order tracking numbers to WordPress admin and customer emails.
 * Version: 1.0
 * Author: Sam McKay
 * Author URI: https://samm.xyz
 * License: MIT
 */


add_action('woocommerce_admin_order_data_after_order_details', 'smOrderTrackingAddMeta');
add_action('woocommerce_email_order_meta', 'smOrderTrackingAddCodeToEmail', 10, 3);
add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'add_action_links');
add_action('woocommerce_process_shop_order_meta', 'smOrderTrackingSaveMeta');


require plugin_dir_path(__FILE__) . 'sm-order-tracking-settings.php';


/**
 * Add action links for plugins page
 */
function add_action_links($links)
{
    $links[] = '<a href="' . admin_url('options-general.php?page=sm_woocommerce_order_tracking_number') . '">Order Tracking Settings</a>';

    return $links;
}


/**
 * Add order tracking textbox to WordPress admin
 */
function smOrderTrackingAddMeta($order)
{
    $orderTrackingNumber = get_post_meta($order->get_order_number(), 'order_tracking_number', true);
    ?>
	<br class="clear" />
	<br class="clear" />
    
    <h4>Order Tracking Info <a href="#" class="edit_address">Edit</a></h4>
	
    <div class="address">
        <p><strong>Tracking Number</strong><?php echo $orderTrackingNumber; ?></p>
    </div>
    
    <div class="edit_address">
        <?php
        woocommerce_wp_text_input(array(
            'id' => 'order_tracking_number',
            'label' => 'Tracking Number',
            'value' => $orderTrackingNumber,
            'wrapper_class' => 'form-field-wide'
        ));
        ?>
    </div>
<?php 
}


/**
 * Tell WordPress / WooCommerce how to save our new order tracking number
 */
function smOrderTrackingSaveMeta($orderId)
{
    update_post_meta($orderId, 'order_tracking_number', wc_clean($_POST['order_tracking_number']));
}


/**
 * Add order tracking code to order email
 */
function smOrderTrackingAddCodeToEmail($orderObj, $sentToAdmin, $plainText)
{
    $trackingCode = get_post_meta($orderObj->get_order_number(), 'order_tracking_number', true);
    $trackingOptions = get_option('sm_order_tracking_options');
    $courier = $trackingOptions['sm_order_tracking_courier'];
    $trackingLink = $trackingOptions['sm_order_tracking_courier_tracking_page'];

    if (!$trackingCode) {
        return;
    }

    if ($plainText) {
        echo 'Courier: ' . $courier ? $courier : 'N/A';
        echo 'Tracking Link: ' . $trackingLink ? $trackingLink : 'N/A';
        echo 'Tracking Code:' . $trackingCode;
    } else {
        echo '<h2>Tracking Information</h2>';
        echo '<p>';
        echo '<strong>Courier:</strong>&nbsp;' . ($courier ? $courier : 'N/A');
        echo '<br><strong>Tracking Link:</strong>&nbsp;<a href="' . ($trackingLink ? $trackingLink : '') . '">' . ($trackingLink ? $courier : 'N/A') . '</a>';
        echo '<br><strong>Tracking Code:</strong>&nbsp;' . $trackingCode;
        echo '</p>';
    }
}