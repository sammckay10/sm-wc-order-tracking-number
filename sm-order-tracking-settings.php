<?php


add_action('admin_init', 'smOrderTrackingRegisterSettings');
add_action('admin_menu', 'smOrderTrackingAddMenuItem');


/**
 * Register the settings page
 */
function smOrderTrackingAddMenuItem()
{
    add_submenu_page('options-general.php', 'Order Tracking', 'Order Tracking', 'manage_options', 'sm_woocommerce_order_tracking_number', 'smOrderTrackingOptionsPage');
}


/**
 * Register the settings fields
 */
function smOrderTrackingRegisterSettings()
{
    register_setting('pluginPage', 'sm_order_tracking_options');

    add_settings_section(
        'sm_pluginPage_section',
        false,
        false,
        'pluginPage'
    );

    add_settings_field(
        'sm_order_tracking_courier',
        __('Courier', 'sm'),
        'smOrderTrackingCourierRender',
        'pluginPage',
        'sm_pluginPage_section'
    );

    add_settings_field(
        'sm_order_tracking_courier_tracking_page',
        __('Courier Tracking Page Link', 'sm'),
        'smOrderTrackingCourierLinkRender',
        'pluginPage',
        'sm_pluginPage_section'
    );
}


function smOrderTrackingCourierRender()
{
    $options = get_option('sm_order_tracking_options');

    ?>
	<input type='text' name='sm_order_tracking_options[sm_order_tracking_courier]' value='<?php echo $options['sm_order_tracking_courier']; ?>'>
	<?php

}


function smOrderTrackingCourierLinkRender()
{
    $options = get_option('sm_order_tracking_options');

    ?>
	<input type='text' name='sm_order_tracking_options[sm_order_tracking_courier_tracking_page]' value='<?php echo $options['sm_order_tracking_courier_tracking_page']; ?>'>
	<?php

}


/**
 * Render the settings form
 */
function smOrderTrackingOptionsPage()
{
    ?>
	<form action='options.php' method='post'>
		<h2>WooCommerce Order Tracking</h2>

        <?php settings_fields('pluginPage'); ?>
        <?php do_settings_sections('pluginPage'); ?> 
        <?php submit_button(); ?>
	</form>
	<?php

}